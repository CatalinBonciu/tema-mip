package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LoginController implements Initializable {

	private String email = "catalinbonciu@yahoo.com";
	private String password = "1234567890";
	
	@FXML
	private Label titleLabel;
	
	@FXML
	private Label emailLabel;
	
	@FXML
	private Label passwordLabel;
	
	@FXML
	private TextField emailTextField;
	
	@FXML
	private PasswordField passwordField;
	
	@FXML
	private Button loginButton;
	
	@FXML
	public void start(Stage primaryStage) {
		
		primaryStage.setTitle("Pet Clinic Login");
		
		Label emailLabel = new Label("Email");
		
		final TextField emailTextField = new TextField();
		
		Label passwordLabel = new Label("Password");
		
		final PasswordField passwordField = new PasswordField();
		
		Button loginButton = new Button("Login");
		
		final Label titleLabel = new Label();
		
	}
	
	@FXML
	private void loginButtonPressed() throws IOException{
		
		if (emailTextField.getText().equals(email) || emailTextField.getText().equals("")) {
			
			if (passwordField.getText().equals(password) || emailTextField.getText().equals("")) {
				
				FXMLLoader fxmlLoader = new FXMLLoader();
				
				fxmlLoader.setLocation(getClass().getResource("MainView.fxml"));
				
				Scene newScene = new Scene(fxmlLoader.load(), 600, 600);
				
				Stage newStage = new Stage();
				
				newStage.setScene(newScene);
				
				newStage.show();
			}
			else {
				passwordLabel.setText("Incorrect password!");
			}
		}
		else  {
			emailLabel.setText("Incorrect email address!");
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
	
		try {
			
			loginButtonPressed();	
		} catch (Exception currentException) {
			
			currentException.printStackTrace();
		}
	}

}
