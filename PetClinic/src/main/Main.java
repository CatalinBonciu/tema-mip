package main;

import java.util.Scanner;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Medicalstaff;
import util.DatabaseUtil;

/**
 * @author Catalin
 *
 */
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/Login.fxml"));
			Scene scene = new Scene(root,800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		dbUtil.setUp();
		Scanner userInput = new Scanner(System.in);
		int usersChoice = 1;
		int AnimalID = 1;
		int DoctorID = 1;
		int AppointmentID = 1;
		launch(args);
		
		while(usersChoice != 0) {
			System.out.println("These are your options : ");
			System.out.println("1) Create an animal! ");
			System.out.println("2) Read all animals! ");
			System.out.println("3) Update an animal! ");
			System.out.println("4) Delete an animal! ");
			System.out.println("5) Create a doctor! ");
			System.out.println("6) Read all doctors! ");
			System.out.println("7) Update a doctor! ");
			System.out.println("8) Delete a doctor! ");
			System.out.println("9) Create an appointment! ");
			System.out.println("10) Read all appointments! ");
			System.out.println("11) Update an appointment! ");
			System.out.println("12) Delete an appointment! ");
			System.out.println("13) Sort appointments by date! ");
			System.out.println("0) Exit! ");
			
			usersChoice = userInput.nextInt();
			/**
			 * @param This switch function offers you the possibility to choose what action you want to take 
			 * on the tables that you have in your database!
			 * 
			 */
			switch(usersChoice) {
				case 1:{
					dbUtil.createAnimal(dbUtil,AnimalID);
					AnimalID++;
					break;
				}
				case 2:{
					dbUtil.readAllAnimalsDB();
					break;
				}
				case 3:{
					System.out.println("Insert the ID of the animal that you want to be updated! : ");
					int idAnimal = userInput.nextInt();
					dbUtil.updateAnimal(idAnimal, dbUtil);
					break;
				}
				case 4:{
					System.out.println("Insert the ID of the animal that you want to be deleted! : ");
					int idAnimal = userInput.nextInt();
					dbUtil.deleteAnimal(idAnimal, dbUtil);
					break;
				}
				case 5:{
					dbUtil.createMedicalStaff(dbUtil,DoctorID);
					DoctorID++;
					break;
				}
				case 6:{
					dbUtil.readAllMedicalStaffDB();
					break;
				}
				case 7:{
					System.out.println("Insert the ID of the doctor that you want to be updated! : ");
					int idDoctor = userInput.nextInt();
					dbUtil.updateMedicalStaff(idDoctor, dbUtil);
					break;
				}
				case 8:{
					System.out.println("Insert the ID of the doctor that you want to be deleted! : ");
					int idDoctor = userInput.nextInt();
					dbUtil.deleteMedicalStaff(idDoctor, dbUtil);
					break;
				}
				case 9:{
					Animal animal = new Animal();
					Medicalstaff doctor = new Medicalstaff();
					dbUtil.createAppointment(dbUtil, animal, doctor,AppointmentID);
					AppointmentID++;
					break;
				}
				case 10:{
					dbUtil.readAllAppointmentsDB();
					break;
				}
				case 11:{
					System.out.println("Insert the ID of the appointment that you want to be updated! : ");
					int idAppointment = userInput.nextInt();
					dbUtil.updateAppointment(idAppointment, dbUtil);;
					break;
				}
				case 12:{
					System.out.println("Insert the ID of the appointment that you want to be deleted! : ");
					int idAppointment = userInput.nextInt();
					dbUtil.deleteAppointment(idAppointment, dbUtil);;
					break;
				}
				case 13:
				{
					dbUtil.sortAppointments();
					break;
				}
				case 0:{
					System.out.println("----Application terminated!----");
					break;
				}
			}
		}
		dbUtil.startTransaction();
		dbUtil.commitTransaction();
		dbUtil.closeEntityManager();
	}

}
