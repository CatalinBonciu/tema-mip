package util;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Animal;
import model.Appointment;
import model.Medicalstaff;

/**
 * @author Catalin
 *
 */


public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public void setUp(){
		entityManagerFactory = Persistence.createEntityManagerFactory("PetClinic");
		entityManager = entityManagerFactory.createEntityManager();
	}
	/**
	 *
	 * @param The createAnimal function uses the database and an ID that are
	 * used to create an animal with all of its atributes.The ID is used not to make
	 * any copies of the animal.(The ID atribute is set as a primary key in the
	 * database).
	 *
	 */
	public void createAnimal(DatabaseUtil dbUtil, int AnimalID) {
		System.out.println("Enter the number of the animals : ");
		Scanner userInput = new Scanner(System.in);
		int numberOfAnimals = userInput.nextInt();
		System.out.println("Enter the Name,Breed,Age and the number of appointments of your pet : ");
		Animal animal = new Animal();
		animal.setIdanimal(AnimalID);
		String breed, name;
		name = userInput.next();
		animal.setName(name);
		breed = userInput.next();
		animal.setBreed(breed);
		int age = userInput.nextInt();
		animal.setAge(age);
		dbUtil.startTransaction();
		entityManager.persist(animal);
		System.out.println("Animal with ID : " + AnimalID + " added to database! ");
		dbUtil.commitTransaction();
	}
	/**
	 *
	 * @param The createAppointment function uses the database,an animal object,a medicalstaff object,and an ID that are
	 * used to create an appointment with all of its atributes.The ID is used not to make
	 * any copies of the appointment.(The ID atribute is set as a primary key in the
	 * database)(The animal and medicalstaff atributes are used to link the tables(OneToMany connection)).
	 *
	 */
	public void createAppointment(DatabaseUtil dbUtil, Animal animal, Medicalstaff doctor, int AppointmentID) {
		System.out.println("Enter the number of appointments");
		Scanner userInput = new Scanner(System.in);
		int numberOfAppointments = userInput.nextInt();
		System.out.println("Enter the Date and the Name of the Clinic of your appointment : ");
		Appointment appointment = new Appointment();
		appointment.setIdappointment(AppointmentID);
		String date, nameOfClinic;
		date = userInput.next();
		nameOfClinic = userInput.next();
		appointment.setDate(date);
		appointment.setClinic(nameOfClinic);
		appointment.setAnimal(animal);
		appointment.setMedicalstaff(doctor);
		entityManager.persist(appointment);
		System.out.println("Appointment with ID : " + AppointmentID + " added to database! ");
		dbUtil.commitTransaction();
	}
	/**
	 *
	 * @param The createMedicalStaff function uses the database and an ID that are 
	 * used to create a medicalstaff object with all of its atributes.The ID is used not to make
	 * any copies of the medicalstaff.(The ID atribute is set as a primary key in the
	 * database).
	 *
	 */
	public void createMedicalStaff(DatabaseUtil dbUtil, int DoctorID) {
		System.out.println("Enter the number of the doctors : ");
		Scanner userInput = new Scanner(System.in);
		int numberOfDoctors = userInput.nextInt();
		dbUtil.startTransaction();
		System.out.println("Enter the First Name,Last Name,Name of Clinic,Age of the doctor : ");
		Medicalstaff doctor = new Medicalstaff();
		String firstName, lastName, nameOfClinic;
		doctor.setIdmedicalstaff(DoctorID);
		firstName = userInput.next();
		doctor.setFirstName(firstName);
		lastName = userInput.next();
		doctor.setLastName(lastName);
		nameOfClinic = userInput.next();
		doctor.setClinic(nameOfClinic);
		int age = userInput.nextInt();
		doctor.setAge(age);
		entityManager.persist(doctor);
		System.out.println("Doctor with ID : " + DoctorID + " added to database! ");
		dbUtil.commitTransaction();
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	public void closeEntityManager() {
		entityManager.close();
	}
	/**
	 *
	 * @param The readAllAnimalsDB function is used to print in the 
	 * console all the animals.
	 *
	 */
	public void readAllAnimalsDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from petclinic.Animal", Animal.class)
				.getResultList();

		for (Animal animal : results) {
			System.out.println("Animal : " + animal.getName() + " breed " + animal.getBreed() + " with the age "
					+ animal.getAge() + " has ID " + animal.getIdanimal());
		}
	}
	/**
	 *
	 * @param The readAllAppointmentsDB function is used to print in the 
	 * console all the appointments.
	 *
	 */
	public void readAllAppointmentsDB() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from petclinic.Appointment", Appointment.class).getResultList();

		for (Appointment appointment : results) {
			System.out.println("Appointment with the ID :  " + appointment.getIdappointment() + " on the date : "
					+ appointment.getDate() + " at the doctor " + appointment.getMedicalstaff());
		}
	}
	public void sortAppointments() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from petclinic.Appointment order by date asc", Appointment.class).getResultList();

		for (Appointment appointment : results) {
			System.out.println("Appointment with the ID :  " + appointment.getIdappointment() + " on the date : "
					+ appointment.getDate() + " at the doctor " + appointment.getMedicalstaff());
		}
	}
	/**
	 *
	 * @param The readAllMedicalStaffDB function is used to print in the 
	 * console all the MedicalStaff.
	 *
	 */
	public void readAllMedicalStaffDB() {
		List<Medicalstaff> results = entityManager
				.createNativeQuery("Select * from petclinic.Medicalstaff", Medicalstaff.class).getResultList();

		for (Medicalstaff medicalStaff : results) {
			System.out.println("Doctor : " + medicalStaff.getFirstName() + medicalStaff.getLastName() + " with ID  "
					+ medicalStaff.getIdmedicalstaff() + " at the age of " + medicalStaff.getAge() + " from clinic "
					+ medicalStaff.getClinic());
		}
	}
	/**
	 *
	 * @param The updateAnimal function is used to update a certain animal
	 * that is in the table.The function uses as parameters an ID that is read
	 * from console and the database.The function uses DatabaseUtil.entityManager.find(Animal.class, idAnimal)
	 * to find the animal with that specific id in the table , and then update its
	 * atributes with new ones that are read from console by the user.
	 *
	 */
	public void updateAnimal(int idAnimal, DatabaseUtil dbUtil) throws Exception {
		Animal animal = DatabaseUtil.entityManager.find(Animal.class, idAnimal);
		dbUtil.startTransaction();
		System.out.println("Enter the new Name,Breed and Age of your pet : ");
		String newName, newBreed;
		int newAge;
		Scanner userInput = new Scanner(System.in);
		newName = userInput.next();
		newBreed = userInput.next();
		newAge = userInput.nextInt();
		animal.setName(newName);
		animal.setAge(newAge);
		animal.setBreed(newBreed);
		System.out.println("Animal with ID : " + idAnimal + " updated from database! ");
		dbUtil.commitTransaction();
	}
	/**
	 *
	 * @param The deleteAnimal function is used to delete a certain animal
	 * recognised by the ID that is sent as a parameter.
	 *
	 */
	public void deleteAnimal(int idAnimal, DatabaseUtil dbUtil) throws Exception {
		Animal animal = DatabaseUtil.entityManager.find(Animal.class, idAnimal);
		dbUtil.startTransaction();
		DatabaseUtil.entityManager.remove(animal);
		System.out.println("Animal with ID : " + idAnimal + " deleted from database! ");
		dbUtil.commitTransaction();
	}
	/**
	 * 
	 * @param The updateAppointment function is used to update a certain appointment
	 * that is in the table.The function uses as parameters an ID that is read
	 * from console and the database.The function uses DatabaseUtil.entityManager.find(Appointment.class, idAppointment)
	 * to find the appointment with that specific id in the table , and then update its
	 * atributes with new ones that are read from console by the user.
	 *
	 */
	// Se poate adauga in Workbench un nou camp fara a se strica tot codul?
	public void updateAppointment(int idAppointment, DatabaseUtil dbUtil) {
		Appointment appointment = DatabaseUtil.entityManager.find(Appointment.class, idAppointment);
		// Animal animal=DatabaseUtil.entityManager.find(Animal.class, idAnimal);
		System.out.println("Enter the new Date,Clinic and doctor ID of your appointment : ");
		dbUtil.startTransaction();
		Scanner userInput = new Scanner(System.in);
		String newClinic = userInput.next();
		appointment.setClinic(newClinic);
		String Date = userInput.nextLine();
		appointment.setDate(Date);
		// appointment.setAnimal(animal);
		System.out.println("Appointment with ID : " + idAppointment + " updated from database! ");
		dbUtil.commitTransaction();
	}
	/**
	 *
	 * @param The deleteAppointment function is used to delete a certain appointment
	 * recognised by the ID that is sent as a parameter.
	 *
	 */
	public void deleteAppointment(int idAppointment, DatabaseUtil dbUtil) throws Exception {
		Appointment appointment = DatabaseUtil.entityManager.find(Appointment.class, idAppointment);
		dbUtil.startTransaction();
		DatabaseUtil.entityManager.remove(appointment);
		System.out.println("Appointment with ID : " + idAppointment + " deleted from database! ");
		dbUtil.commitTransaction();
	}
	/**
	 * 
	 * @param The updateMedicalStaff function is used to update a certain doctor
	 * that is in the table.The function uses as parameters an ID that is read
	 * from console and the database.The function uses DatabaseUtil.entityManager.find(Medicalstaff.class, idMedicalStaff)
	 * to find the doctor with that specific id in the table , and then update its
	 * atributes with new ones that are read from console by the user.
	 *
	 */
	public void updateMedicalStaff(int idMedicalStaff, DatabaseUtil dbUtil) {
		Medicalstaff doctor = DatabaseUtil.entityManager.find(Medicalstaff.class, idMedicalStaff);
		System.out.println("Enter the new First Name, Last Name,Clinic ,Age and new Appointment ID : ");
		dbUtil.startTransaction();
		Scanner userInput = new Scanner(System.in);
		String newFirstName, newLastName, newClinic;
		int newAge, newAppointmentID;
		newFirstName = userInput.next();
		newLastName = userInput.next();
		newClinic = userInput.next();
		newAge = userInput.nextInt();
		newAppointmentID = userInput.nextInt();
		doctor.setFirstName(newFirstName);
		doctor.setLastName(newLastName);
		doctor.setAge(newAge);
		doctor.setClinic(newClinic);
		doctor.setIdAppointment(newAppointmentID);
		System.out.println("Doctor with ID : " + idMedicalStaff + " updated from database! ");
		// doctor.setAppointments();
		dbUtil.commitTransaction();
	}
	/**
	 *
	 * @param The deleteMedicalStaff function is used to delete a certain doctor
	 * recognised by the ID that is sent as a parameter.
	 *
	 */
	public void deleteMedicalStaff(int idMedicalStaff, DatabaseUtil dbUtil) throws Exception {
		Medicalstaff doctor = DatabaseUtil.entityManager.find(Medicalstaff.class, idMedicalStaff);
		dbUtil.startTransaction();
		DatabaseUtil.entityManager.remove(doctor);
		System.out.println("Doctor with ID : " + idMedicalStaff + " deleted from database! ");
		dbUtil.commitTransaction();
	}
	public List<Animal> animalList (){
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}
}