package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medicalstaff database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalstaff.findAll", query="SELECT m FROM Medicalstaff m")
public class Medicalstaff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idmedicalstaff;

	private int age;

	private String clinic;

	private String firstName;

	private int idAppointment;

	private String lastName;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="medicalstaff")
	private List<Appointment> appointments;

	public Medicalstaff() {
	}

	public int getIdmedicalstaff() {
		return this.idmedicalstaff;
	}

	public void setIdmedicalstaff(int idmedicalstaff) {
		this.idmedicalstaff = idmedicalstaff;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getClinic() {
		return this.clinic;
	}

	public void setClinic(String clinic) {
		this.clinic = clinic;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getIdAppointment() {
		return this.idAppointment;
	}

	public void setIdAppointment(int idAppointment) {
		this.idAppointment = idAppointment;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalstaff(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalstaff(null);

		return appointment;
	}

}