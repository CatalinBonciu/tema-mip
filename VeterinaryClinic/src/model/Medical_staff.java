package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the `medical staff` database table.
 * 
 */
@Entity
@Table(name="`medical staff`")
@NamedQuery(name="Medical_staff.findAll", query="SELECT m FROM Medical_staff m")
public class Medical_staff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="`idMedical Staff`")
	private int idMedical_Staff;

	private String medicalStaffName;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="medicalStaff")
	private List<Appointment> appointments;

	public Medical_staff() {
	}

	public int getIdMedical_Staff() {
		return this.idMedical_Staff;
	}

	public void setIdMedical_Staff(int idMedical_Staff) {
		this.idMedical_Staff = idMedical_Staff;
	}

	public String getMedicalStaffName() {
		return this.medicalStaffName;
	}

	public void setMedicalStaffName(String medicalStaffName) {
		this.medicalStaffName = medicalStaffName;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalStaff(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalStaff(null);

		return appointment;
	}

}