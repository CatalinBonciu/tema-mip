package util;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.Medical_staff;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("VeterinaryClinic");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	
	public void saveAppointment(Appointment appointment) {
		entityManager.persist(appointment);
	}
	
	public void saveMedicalStaff(Medical_staff doctor) {
		entityManager.persist(doctor);
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager() {
		entityManager.close();
	}
	
	public void printAllAnimalsDB() {
		List<Animal> results=entityManager.createNativeQuery("Select * from veterinaryclinic.Animal",Animal.class).getResultList();
		
		for(Animal animal : results) {
			System.out.println("Animal : " + animal.getName() + " has ID : " + animal.getIdAnimal());
		}
	}
	
	public void printAllAppointmentsDB() {
		List<Appointment> results=entityManager.createNativeQuery("Select * from veterinaryclinic.Appointment",Appointment.class).getResultList();
		
		for(Appointment appointment : results) {
			System.out.println("Appointment with the ID :  " + appointment.getIdAppointment() + "on the date : " + appointment.getAppointmentDate() + " at the doctor " + appointment.getDoctorName());
		}
	}
	
	public void printAllMedicalStaffDB() {
		List<Medical_staff> results=entityManager.createNativeQuery("Select * from veterinaryclinic.Medical_staff",Medical_staff.class).getResultList();
		
		for(Medical_staff medicalStaff : results) {
			System.out.println("Doctor : " + medicalStaff.getMedicalStaffName() + " with ID : " + medicalStaff.getIdMedical_Staff());
		}
	}
	
	public void updateAnimal(Animal animal,String newName,int newId) {
		animal.setName(newName);
		animal.setIdAnimal(newId);
	}
	
	public void deleteAnimal(Animal animal) {
		entityManager.remove(animal);
	}
	
	public void updateAppointment(Appointment appointment,String newDoctorName,int newId,Date newDate) {
		appointment.setDoctorName(newDoctorName);
		appointment.setIdAppointment(newId);
		appointment.setAppointmentDate(newDate);
	}
	
	public void deleteAppointment(Appointment appointment) {
		entityManager.remove(appointment);
	}
	
	public void updateMedicalStaff(Medical_staff doctor,String newName,int newId) {
		doctor.setMedicalStaffName(newName);
		doctor.setIdMedical_Staff(newId);
	}
	
	public void deleteMedicalStaff(Medical_staff doctor) {
		entityManager.remove(doctor);
	}
}
