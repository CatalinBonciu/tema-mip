package main;

import model.Animal;
import util.DatabaseUtil;

public class Main {

	public static void main(String[] args) throws Exception {
		DatabaseUtil dbUtil = new DatabaseUtil();
		Animal animal = new Animal();
		animal.setIdAnimal(3);
		animal.setName("Dogggo");
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(animal);
		dbUtil.commitTransaction();
		dbUtil.deleteAnimal(animal);
		dbUtil.printAllAnimalsDB();
		dbUtil.closeEntityManager();
	}

}
